Hyperspace is a documentation module that will point all your admin links in your Drupal documentation pages to a site that you set!

This is an easy way to make your documentation pages interact with the site that you are currently working on! 

Setting this up is simple:
In your documentation pages, simple use this format for writing links

<!-- sample hyperspace link denotation -->
<span class="admin-path">admin/settings/hyperspace</span>
Just add an "admin-path" class to any element (I use span because it is usually inline text) and write that path without an initial forward slash


Enabling the module is also simple:

<ol>
	<li><strong>Download</strong> the module and un tar it</li>
	<li><strong>Place</strong> it in the /sites/all/modules directory</li>
	<li><strong>Enable</strong> it admin/build/modules</li>
	<li><strong>Navigate to</strong> admin/build/block and place the hyperspace module in a region</li>
	<li>You will be <strong>prompted</strong> to "Please enter your first site URL" -> whereby, you click that link and enter a Drupal site path(eg: "http://www.example.com")</li>
	<li><strong>Submit</strong> the form</li>
</ol>

Now you have prepared the module to search for paths! Hyperspace uses jQuery to transform all ".admin-path" elements into live links. 