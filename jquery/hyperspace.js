$(document).ready(function(){
  //if the "hyperspace-path" exists, grab the contents and save it as a local variable
   var $devpath = $("#hyperspace-path").text();
   //alert("dev path is "+$devpath);
  // for every instance of "admin-path" , rewrite the path that is just text... and replace it with a link (using the dev path)
   $(".admin-path"). each(function(){
   //alert("we are inside with  "+$devpath);
   // snag the text inside this element and place it in a variable
     var $relativepath = $(this).text();
    //alert("we are inside with  "+$relativepath);
    // now rewrite the contents of this with the dev path and the relative path
    $(this).replaceWith("<a href=\"" +$devpath+$relativepath +"\" target=\"_blank\">"+$relativepath+"</a>");  
   } 
  );  
});